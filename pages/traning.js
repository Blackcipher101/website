import React from 'react'
import Layout from '../components/layout';
import android from "../public/android.png";
import TraningSections from '../components/home/traningSections';
export default () => {
    const styling = {
      backgroundImage: `url('../../photos/traning.jpg')`,
    };
  
    return (
        <Layout>
      <div id="home-landing" style={styling}>
        <div className="overlay">
          <div id="landing-btn">
            <a
              href="#home-section-1"
              className="fa fa-chevron-down animated infinite bounceOutDown delay-2s slow"
            />
          </div>
          <div className="col-6" id="landing-info">
                <div className="d-flex align-items-center text-lg-left text-center justify-content-center h-100">
                  <div>
                    <img src={android} width="160" height="230"/>
                    <p></p>
                    <h1>Android</h1>
                    <h3>Devlepmoent</h3>
                    <h6>Course designed by Abhishek J M</h6>
                    <button className="btn btn-lg button heading-font bg-light">
                      <a href="#registration-form" style={{color: 'black', textDecoration: 'none'}}>REGISTER NOW</a>
                    </button>
                  </div>
                </div>
            </div>
        
        </div>
        </div>
        
        <div style={{ background: '#ffe' }}>
        <h1 className="text-center h-100">Course Mentors</h1>
        <TraningSections />
        </div>
      </Layout>
    );
  };